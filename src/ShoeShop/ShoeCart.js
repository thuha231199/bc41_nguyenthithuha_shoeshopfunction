import React from 'react'

function ShoeCart({shoeCart , handleChangeQuantity , handleDelete}) {
    console.log(shoeCart);
    let renderShoeCart = () => {
        return shoeCart.map((shoe , index)=> {
            let {id , name , price , soLuong , image} = shoe ; 
            return (
                <tr key={index}>
                    <td>{id}</td>
                    <td>{name}</td>
                    <td>{price}</td>
                    <td><button onClick={() => {
                        handleChangeQuantity(id , -1)
                    }} className="btn btn-outline-info btn-sm">-</button><strong className='mx-2'>{soLuong}</strong><button onClick={() => {
                        handleChangeQuantity(id , 1)
                    }} className="btn btn-outline-info btn-sm">+</button></td>
                    <td>{price * soLuong}</td>
                    <td><img src={image} alt='shoe_img' style={{width : '50px'}}/></td>
                    <td><button onClick={() => {
                        handleDelete(id) ; 
                    }} className="btn btn-danger">Delete</button></td>
                </tr>
            )
        })
    }
    let renderTotal = () => { 
        let total = 0  ;
        shoeCart.forEach(shoe => total += shoe.price * shoe.soLuong) ; 
        return total ; 
        
    }
    if (shoeCart.length > 0) {
        return (        
            <div className='container p-5'>
                <table class="table table-inverse table-inverse">
                    <thead class="thead-inverse">
                        <tr>
                            <th>ID</th>
                            <th>NAME</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>TOTAL</th>
                            <th>IMAGE</th>
                            <th>ACTION</th>
                        </tr>
                        </thead>
                        <tbody>
                            {renderShoeCart()}
                        </tbody>
                </table>
                <p className='text-center'>Tổng số tiền là :  <strong>{renderTotal()}</strong></p>
            </div>
        )
    }
}

export default ShoeCart