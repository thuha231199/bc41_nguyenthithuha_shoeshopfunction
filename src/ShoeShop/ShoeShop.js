import React from 'react'
import ListShoe from './ListShoe'
import ShoeCart from './ShoeCart'
import { data_shoe } from './data_shoe'
import { useState } from 'react'

function ShoeShop() {
    const [listShoe] = useState(data_shoe) ; 
    const [shoeCart, setshoeCart] = useState([]) ; 
    let handleAddtoCart = (shoe) => {
        let shoeCartClone = [...shoeCart] ; 
        let index = shoeCartClone.findIndex(item => item.id === shoe.id) ; 
        if (index === -1) {
            let shoeClone = {...shoe , soLuong : 1} ; 
            shoeCartClone.push(shoeClone) ; 
        }else {
            shoeCartClone[index].soLuong += 1 ; 
        }
        setshoeCart(shoeCartClone) ; 
    }
    let handleChangeQuantity = (id , luaChon) => {
        let shoeCartClone = [...shoeCart] ; 
        let index = shoeCartClone.findIndex(item => item.id === id) ; 
        shoeCartClone[index].soLuong =  shoeCartClone[index].soLuong + luaChon ; 
        setshoeCart(shoeCartClone) ; 
    }
    let handleDelete = (id) => {
        let shoeCartClone = [...shoeCart] ;
        let filteredShoe = shoeCartClone.filter(shoe => shoe.id !== id) ; 
        setshoeCart(filteredShoe) ;  
    }
  return (
    <div>
        <ShoeCart shoeCart = {shoeCart} handleChangeQuantity = {handleChangeQuantity} handleDelete = {handleDelete} />
        <ListShoe listShoe = {listShoe} handleAddtoCart = {handleAddtoCart}/>
    </div>
  )
}

export default ShoeShop