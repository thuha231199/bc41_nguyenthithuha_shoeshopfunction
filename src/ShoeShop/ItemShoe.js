import React from 'react'
import { Card , Button } from 'antd'
const { Meta } = Card;
function ItemShoe({shoe , handleAddtoCart}) {
    console.log(shoe);
  return (
    <div className='col-4 mb-5'>
        <Card
        hoverable
        style={{ width: "100%" }}
        cover={<img style={{width : '100%' , objectFit : "cover" , height : '200px'}} alt="example" src={shoe.image} />}
        >
            <Meta title={shoe.name} description = {shoe.price} />
            <Button onClick={() => {
                handleAddtoCart(shoe) ; 
            }} type = "primary mt-5">Add to Cart</Button>
        </Card>
    </div>
  )
}

export default ItemShoe